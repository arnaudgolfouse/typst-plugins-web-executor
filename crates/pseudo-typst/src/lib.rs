use std::{collections::HashMap, sync::Mutex};
use wasm_bindgen::prelude::wasm_bindgen;
use wasm_plugin_protocol::Plugin;

static PLUGINS: Mutex<Option<HashMap<String, Plugin>>> = Mutex::new(None);

#[wasm_bindgen]
pub async fn load_and_run(input: &str) {
    let _ = console_log::init();

    if let Err(err) = inner(input).await {
        log::error!("Error: {err}");
    }
}

pub async fn inner(input: &str) -> Result<(), String> {
    let path = "./plugins/plugin.wasm";

    let plugin = {
        let list = PLUGINS.lock().unwrap();
        list.iter().flatten().find_map(|(name, plugin)| {
            if name == path {
                Some(plugin.clone())
            } else {
                None
            }
        })
    };
    let plugin = match plugin {
        Some(p) => p,
        None => {
            let new_plugin = Plugin::new(path).await?;
            PLUGINS
                .lock()
                .unwrap()
                .get_or_insert(HashMap::new())
                .insert(path.to_owned(), new_plugin.clone());
            new_plugin
        }
    };

    let output = String::from_utf8(plugin.call("double_it", vec![input.into()])?).unwrap();

    let window = web_sys::window().expect("no global `window` exists");
    let document = window.document().expect("should have a document on window");

    document
        .get_element_by_id("wasm-input")
        .unwrap()
        .set_inner_html(&format!("WASM input: '{}'", input));
    document
        .get_element_by_id("wasm-output")
        .unwrap()
        .set_inner_html(&format!("WASM output: '{}'", output));
    Ok(())
}
