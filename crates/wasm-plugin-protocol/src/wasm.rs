use crate::MemoryError;
use js_sys::{Object, Reflect, Uint8Array, WebAssembly};
use std::{
    cell::RefCell,
    collections::HashMap,
    ops::DerefMut,
    rc::Rc,
    sync::{Arc, Mutex},
};
use wasm_bindgen::{closure::Closure, JsCast, JsValue};
use wasm_bindgen_futures::JsFuture;

#[derive(Clone)]
pub struct Plugin(Arc<Mutex<Repr>>);

struct Repr {
    plugin_instance: WebAssembly::Instance,
    functions: HashMap<String, js_sys::Function>,
    store_data: Rc<RefCell<StoreData>>,
    // These functions need to be alive for the lifetime of the plugin.
    _wasm_minimal_protocol_write_args_to_buffer: Closure<dyn FnMut(u32)>,
    _wasm_minimal_protocol_send_result_to_host: Closure<dyn FnMut(u32, u32)>,
}

/// Safety:
/// - `Repr` is not `Send` (nor `Sync`) 😨 !
/// - But, every access to it will be done behind a `Mutex` lock.
/// - And we have no way of pulling `Repr` of of it (it does not implement `Clone`), no way to pull any `Repr`-related data in fact.
///
/// So no concurrent reads/writes can actually happen, so we can implement `Send`,
/// and it will be fine !
unsafe impl Send for Repr {}

#[derive(Default)]
struct StoreData {
    memory: Option<WebAssembly::Memory>,
    args: Vec<Vec<u8>>,
    output: Vec<u8>,
    memory_error: Option<MemoryError>,
}

impl Plugin {
    /// Create a new plugin from raw WebAssembly bytes.
    pub async fn new(path: &str) -> Result<Self, String> {
        let window = web_sys::window().unwrap();

        let store_data: Rc<RefCell<StoreData>> = Rc::new(RefCell::new(StoreData::default()));
        let store_data_write_args = store_data.clone();
        let store_data_send_result = store_data.clone();

        let imports = Object::new();
        let typst_env = Object::new();

        // Write the arguments to the plugin function into the plugin's memory.
        let wasm_minimal_protocol_write_args_to_buffer: Closure<dyn FnMut(u32)> =
            Closure::new(move |mut ptr: u32| {
                log::debug!("write_args_to_buffer: ptr = {ptr}");
                let mut store_data = store_data_write_args.borrow_mut();
                let store_data = &mut *store_data;
                let Some(memory) = store_data.memory.as_ref() else {
                    log::error!("ERROR: memory is absent");
                    return;
                };
                let memory = Uint8Array::new(&memory.buffer());
                for arg in &store_data.args {
                    let len = arg.len() as u32;
                    if memory.byte_length() < ptr + len {
                        log::error!("ERROR: write memory error at {ptr}, length {len}");
                        store_data.memory_error = Some(MemoryError {
                            offset: ptr,
                            length: len,
                            write: true,
                        });
                        return;
                    }
                    memory.subarray(ptr, ptr + len).copy_from(arg);
                    ptr += len;
                }
            });

        // Extracts the output of the plugin function from the plugin's memory.
        let wasm_minimal_protocol_send_result_to_host: Closure<dyn FnMut(u32, u32)> =
            Closure::new(move |ptr: u32, len: u32| {
                log::debug!("send_result_to_host: ptr = {ptr}, len = {len}");
                let mut store_data = store_data_send_result.borrow_mut();
                let store_data = &mut *store_data;
                let Some(memory) = &store_data.memory else {
                    log::error!("ERROR: memory is absent");
                    return;
                };
                let memory = Uint8Array::new(&memory.buffer());
                let memory = memory.subarray(ptr, ptr + len);
                if memory.length() != len {
                    log::error!("ERROR: read memory error at {ptr}, length {len}");
                    store_data.memory_error = Some(MemoryError {
                        offset: ptr,
                        length: len,
                        write: false,
                    });
                    return;
                }
                let mut buffer = std::mem::take(&mut store_data.output);
                buffer.resize(len as usize, 0);
                memory.copy_to(&mut buffer);
                store_data.output = buffer;
            });

        Reflect::set(
            &typst_env,
            &JsValue::from_str("wasm_minimal_protocol_write_args_to_buffer"),
            wasm_minimal_protocol_write_args_to_buffer.as_ref(),
        )
        .unwrap();
        Reflect::set(
            &typst_env,
            &JsValue::from_str("wasm_minimal_protocol_send_result_to_host"),
            wasm_minimal_protocol_send_result_to_host.as_ref(),
        )
        .unwrap();
        Reflect::set(&imports, &JsValue::from_str("typst_env"), &typst_env).unwrap();

        let wasm = JsFuture::from(WebAssembly::instantiate_streaming(
            &window.fetch_with_str(path),
            &imports,
        ))
        .await
        .unwrap();

        log::debug!("Creating instance...");

        let instance: WebAssembly::Instance = Reflect::get(&wasm, &"instance".into())
            .unwrap()
            .dyn_into()
            .unwrap();

        let Ok(memory) = Reflect::get(&instance.exports(), &"memory".into()) else {
            return Err(String::from("plugin does not export its memory"));
        };

        store_data.borrow_mut().memory = Some(match memory.dyn_into() {
            Ok(m) => m,
            Err(_) => return Err(String::from("plugin does not export its memory")),
        });

        Ok(Plugin(Arc::new(Mutex::new(Repr {
            plugin_instance: instance,
            functions: HashMap::new(),
            store_data,
            _wasm_minimal_protocol_write_args_to_buffer: wasm_minimal_protocol_write_args_to_buffer,
            _wasm_minimal_protocol_send_result_to_host: wasm_minimal_protocol_send_result_to_host,
        }))))
    }

    pub fn call(&self, name: &str, args: Vec<Vec<u8>>) -> Result<Vec<u8>, String> {
        log::debug!("calling function {name}");
        let mut plugin = self.0.lock().unwrap();
        let plugin = plugin.deref_mut();

        let function_entry = plugin.functions.entry(name.into());

        let mut entry;
        let function = match function_entry {
            std::collections::hash_map::Entry::Occupied(f) => {
                entry = f;
                entry.get_mut()
            }
            std::collections::hash_map::Entry::Vacant(entry) => {
                let f = Reflect::get(&plugin.plugin_instance.exports(), &name.into())
                    .and_then(|f| f.dyn_into::<js_sys::Function>())
                    .map_err(|_| format!("plugin does not contain a function called {name}"))?;
                entry.insert(f)
            }
        };

        {
            let expected = function.length() as usize;
            let given = args.len();
            if expected != given {
                return Err(format!(
                    "plugin function takes {expected} argument{}, but {given} {} given",
                    if expected == 1 { "" } else { "s" },
                    if given == 1 { "was" } else { "were" },
                ));
            }
        }

        let args_lengths = js_sys::Array::from_iter(args.iter().map(|a| JsValue::from(a.len())));
        plugin.store_data.borrow_mut().args = args;

        let result = match function.apply(&JsValue::undefined(), &args_lengths) {
            Ok(r) => r,
            Err(err) => return Err(format!("plugin panicked: {err:?}")),
        };
        let Some(code) = result.as_f64().map(|f| f as i32) else {
            return Err(format!(
                "plugin function `{name}` does not return exactly one 32-bit integer"
            ));
        };

        if let Some(MemoryError {
            offset,
            length,
            write,
        }) = plugin.store_data.borrow_mut().memory_error.take()
        {
            return Err(format!(
                 "plugin tried to {kind} out of bounds: pointer {offset:#x} is out of bounds for {kind} of length {length}",
                 kind = if write { "write" } else { "read" }
            ));
        }

        let output = std::mem::take(&mut plugin.store_data.borrow_mut().output);

        match code {
            0 => {}
            1 => match std::str::from_utf8(&output) {
                Ok(message) => return Err(format!("plugin errored with: {message}")),
                Err(_) => {
                    return Err(String::from(
                        "plugin errored, but did not return a valid error message",
                    ))
                }
            },
            _ => return Err(String::from("plugin did not respect the protocol")),
        };

        Ok(output)
    }
}
