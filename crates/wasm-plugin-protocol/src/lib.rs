//! Propose an interface for instantiating and calling into wasm plugins, following the
//! plugin protocol.
//!
//! Note that the interface is has one slight difference when compiled in native vs wasm:
//! `Plugin::new` becomes `async` in wasm.

#[cfg(target_arch = "wasm32")]
mod wasm;
#[cfg(target_arch = "wasm32")]
pub use wasm::Plugin;

/// If there was an error reading/writing memory, keep the offset + length to
/// display an error message.
#[derive(Clone, Copy, Debug, Default)]
struct MemoryError {
    offset: u32,
    length: u32,
    write: bool,
}

#[cfg(not(target_arch = "wasm32"))]
mod wasmi;

#[cfg(not(target_arch = "wasm32"))]
pub use wasmi::Plugin;
