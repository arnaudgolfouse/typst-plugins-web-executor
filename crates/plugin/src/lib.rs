#[link(wasm_import_module = "typst_env")]
extern "C" {
    #[link_name = "wasm_minimal_protocol_send_result_to_host"]
    fn __send_result_to_host(ptr: *const u8, len: usize);
    #[link_name = "wasm_minimal_protocol_write_args_to_buffer"]
    fn __write_args_to_buffer(ptr: *mut u8);
}

#[no_mangle]
pub extern "C" fn double_it(input_length: usize) -> i32 {
    let mut input = vec![0u8; input_length];
    unsafe {
        __write_args_to_buffer(input.as_mut_ptr());
    }
    let output = input.repeat(2);
    assert_eq!(output.len(), 2 * input_length);
    unsafe { __send_result_to_host(output.as_ptr(), output.len()) }
    0
}
