An experiment (directed at [typst](https://github.com/typst/typst)) to see how a plugin system might use the browser's wasm executor.

# Build and run

To run, you need:

- The [Rust compiler](https://www.rust-lang.org/)
- [`wasm-pack`](https://rustwasm.github.io/wasm-pack/installer/), installable via `cargo install wasm-pack`
- Python 3

Then:

- Run the `build.sh` script
- Run the command `python3 -m http.server`
- Visit `http://0.0.0.0:8000/`, and try putting things in the box.

# Organisation

This is separated in 3 crates:

- [crates/pseudo-typst](./crates/pseudo-typst): a crate taking the role of typst, with basically no features (can only run the plugin's function and display the results). It depends on `wasm-plugin-protocol`.
- [crates/plugin](./crates/plugin): the plugin
- [crates/wasm-plugin-protocol](./crates/wasm-plugin-protocol): Implements the code to load and run plugin functions. This is the meat of the repository.
