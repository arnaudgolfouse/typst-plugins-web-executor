#!/bin/sh

set -ex

cargo build --target wasm32-unknown-unknown --release
mkdir -p ./plugins
cp "./target/wasm32-unknown-unknown/release/plugin.wasm" "./plugins/"
wasm-pack build --target web -d ../../pkg crates/pseudo-typst